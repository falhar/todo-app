const { Op } = require('sequelize');
const ApiError = require('../errors/api-error');
const { task, objective } = require('../models');

class TaskController {
  async getBySearch(req, res) {
    const { Page, Limit, Title, Action_Time_Start, Action_Time_End, Is_Finished } = req.query;
    try {
      const where = {};
      if (Title) {
        where.Title = { [Op.like]: `%${Title}%` };
      }
      if (Action_Time_Start) {
        where.Action_Time = { [Op.gte]: Action_Time_Start };
      }
      if (Action_Time_End) {
        if (Action_Time_Start) {
          where.Action_Time[Op.lte] = Action_Time_End;
        } else {
          where.Action_Time = { [Op.lte]: Action_Time_End };
        }
      }
      if (Is_Finished) {
        where.Is_Finished = Is_Finished === 'true';
      }
      const data = await task.findAll({
        where,
        include: { model: objective, as: 'Objective_List', attributes: ['Objective_Name', 'Is_Finished'] },
        limit: parseInt(Limit, 10),
        offset: parseInt(Limit, 10) * (parseInt(Page, 10) - 1),
      });
      if (!data) {
        throw new ApiError(200, 'Task not found', 'error_param');
      }
      res.status(200).json({
        message: 'Success',
        data: {
          List_Data: data,
        },
        Pagination_Data: {
          Curren_Page: parseInt(Page, 10),
          Max_Data_Per_Page: parseInt(Limit, 10),
          Max_Page: Math.ceil(data.length / parseInt(Limit, 10)),
          Total_All_Data: data.length,
        },
      });
    } catch (error) {
      throw new ApiError(200, error.message, 'error_internal_server');
    }
  }

  async getOne(req, res) {
    const data = await task.findOne({
      where: { Task_ID: req.params.Id },
      include: { model: objective, as: 'Objective_List', attributes: ['Objective_Name', 'Is_Finished'] },
    });
    if (!data) {
      throw new ApiError(200, 'Task Not Found!', 'error_id_not_found');
    }
    res.status(200).json({
      message: 'Success',
      data,
    });
  }

  async addTask(req, res) {
    const { Title, Action_Time, Objective_List } = req.body;
    const newTask = await task.create({ Title, Action_Time });
    Objective_List.map(async (Objective_Name) => {
      await objective.create({ Objective_Name, Task_ID: newTask.Task_ID });
    });
    res.status(200).json({
      message: 'Success',
    });
  }

  async updateTask(req, res) {
    const { Title, Objective_List } = req.body;
    const allClear = Objective_List.filter((data) => data.Is_Finished === false);
    Objective_List.map(async (data) => {
      await objective.destroy({ where: { Task_ID: req.params.Id } });
      await objective.create({ Objective_Name: data.Objective_Name, Is_Finished: data.Is_Finished, Task_ID: req.params.Id });
    });
    await task.update({ Title, Is_Finished: allClear.length === 0 }, { where: { Task_ID: req.params.Id } });
    res.status(200).json({
      message: 'Success',
    });
  }

  async deleteTask(req, res) {
    await task.destroy({ where: { Task_ID: req.params.Id } });
    res.status(200).json({
      message: 'Success',
    });
  }
}

module.exports = new TaskController();
