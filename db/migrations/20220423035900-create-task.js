module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tasks', {
      Task_ID: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true, allowNull: false },
      Title: { type: Sequelize.STRING, allowNull: false },
      Action_Time: { type: Sequelize.DATE, allowNull: false },
      Is_Finished: { type: Sequelize.BOOLEAN, allowNull: false, defaultValue: 0 },
      Created_Time: { allowNull: false, type: Sequelize.DATE },
      Updated_Time: { allowNull: false, type: Sequelize.DATE },
    });
  },
  // eslint-disable-next-line no-unused-vars
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tasks');
  },
};
