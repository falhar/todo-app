module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('objectives', {
      Objective_ID: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true, allowNull: false },
      Task_ID: { type: Sequelize.INTEGER, references: { model: 'tasks', key: 'Task_ID' }, onDelete: 'CASCADE' },
      Objective_Name: { type: Sequelize.STRING, allowNull: false },
      Is_Finished: { type: Sequelize.BOOLEAN, allowNull: false, defaultValue: 0 },
    });
  },
  // eslint-disable-next-line no-unused-vars
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('objectives');
  },
};
