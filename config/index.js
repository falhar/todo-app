const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '../.env') });
const db = require('./db');

module.exports = {
  app: {
    port: process.env.PORT,
    env: process.env.NODE_ENV,
    password_salt: 12,
  },
  db,
  log: {
    level: process.env.LOG_LEVEL,
  },
  jwt: {
    secretKey: process.env.JWT_SECRET_KEY,
    expiresIn: process.env.JWT_EXP_TOKEN,
  },
};
