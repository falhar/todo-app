module.exports = {
  testEnvironment: 'node',
  testEnvironmentOptions: {
    NODE_ENV: 'test',
  },
  setupFilesAfterEnv: ['./tests/setupTest.js'],
  restoreMocks: true,
  testMatch: ['<rootDir>/tests/**/*.test.js'],
  collectCoverageFrom: ['./**/*.js'],
  coveragePathIgnorePatterns: ['node_modules', 'config', 'db', 'models/index.js', 'index.js', 'config.js', 'coverage'],
  coverageReporters: ['html', 'text', 'text-summary', 'cobertura', 'lcov'],
  testResultsProcessor: 'jest-sonar-reporter',
};
