const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Objective extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Objective.belongsTo(models.task, { foreignKey: 'Task_ID' });
    }
  }
  Objective.init(
    {
      Objective_ID: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, allowNull: false },
      Task_ID: { type: DataTypes.INTEGER, references: { model: 'tasks', key: 'Task_ID' } },
      Objective_Name: { type: DataTypes.STRING, allowNull: false },
      Is_Finished: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: 0 },
    },
    {
      sequelize,
      modelName: 'objective',
      createdAt: false,
      updatedAt: false,
    }
  );
  return Objective;
};
