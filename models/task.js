const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Task extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Task.hasMany(models.objective, { foreignKey: 'Task_ID', onDelete: 'CASCADE', as: 'Objective_List' });
    }
  }
  Task.init(
    {
      Task_ID: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, allowNull: false },
      Title: { type: DataTypes.STRING, allowNull: false },
      Action_Time: {
        type: DataTypes.DATE,
        allowNull: false,
        get() {
          const rawValue = this.getDataValue('Action_Time');
          return rawValue ? new Date(rawValue).getTime() / 1000 : rawValue;
        },
      },
      Is_Finished: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: 0 },
      Created_Time: {
        type: DataTypes.DATE,
        allowNull: false,
        get() {
          const rawValue = this.getDataValue('Created_Time');
          return rawValue ? new Date(rawValue).getTime() / 1000 : rawValue;
        },
      },
      Updated_Time: {
        type: DataTypes.DATE,
        allowNull: false,
        get() {
          const rawValue = this.getDataValue('Updated_Time');
          return rawValue ? new Date(rawValue).getTime() / 1000 : rawValue;
        },
      },
    },
    {
      sequelize,
      modelName: 'task',
      createdAt: 'Created_Time',
      updatedAt: 'Updated_Time',
    }
  );
  return Task;
};
