const express = require('express');
const cors = require('cors');
const config = require('./config');
const morgan = require('./tools/morgan');
const taskRoute = require('./routes/task');
const ApiError = require('./errors/api-error');
const errorMiddleware = require('./middlewares/error');

const app = express();

if (config.app.env !== 'test') {
  app.set('trust proxy', true);
  app.use(morgan.logSuccessRequest);
  app.use(morgan.logErrorRequest);
}

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.use('/task', taskRoute);

app.use(async (req, res, next) => next(new ApiError(200, 'Endpoint Not Found!', 'error_internal_server')));

app.use(errorMiddleware);

module.exports = app;
