class ApiError extends Error {
  constructor(
    statusCode,
    message = 'Internal Server Error',
    error_key = 'error_internal_server',
    errorPayloads = {},
    isOperational = true,
    stack = ''
  ) {
    super(message);
    this.statusCode = statusCode;
    this.error_key = error_key;
    this.errorPayloads = errorPayloads;
    this.isOperational = isOperational;
    if (stack) {
      this.stack = stack;
    } else {
      Error.captureStackTrace(this, this.constructor);
    }
  }
}

module.exports = ApiError;
