# To Do App API

## Requirements

1. Nodejs >= 14
2. Mysql 5.7
3. Npm global packages
   - sequelize-cli

## Installation

1. Clone Project
2. Install node dependencies

```
npm Install
or
npm install --only=prod
```

3. Buat file .env dari .env.example

```
cp .env.example .env
```

4. Sesuaikan variabel `.env`
   - variabel `DB_HOST`, `DB_USER`, `DB_PASS`, `DB_DATABASE` sesuai environment
5. Jalankan migrasi

```
sequelize db:migrate
```

6. Aplikasi siap running

```
npm run dev
```

## Deployment Notes

### Running with PM2

1. Install pm2 as global package

```
sudo npm install -g pm2
```

2. Run with pm2

```
cd path/to/app
pm2 start --name="nama aplikasi" node index.js
```
