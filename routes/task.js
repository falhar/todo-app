const router = require('express').Router();
const { getBySearch, getOne, addTask, updateTask, deleteTask } = require('../controllers/task');
const { searchTaskVal, getOrDelOneTaskVal, addTaskVal, updateTaskVal } = require('../middlewares/validators/task');
const catchAsync = require('./catch-async');

router.get('/get', searchTaskVal, catchAsync(getBySearch));
router.get('/get/:Id', getOrDelOneTaskVal, catchAsync(getOne));
router.post('/add', addTaskVal, catchAsync(addTask));
router.put('/update/:Id', updateTaskVal, catchAsync(updateTask));
router.delete('/delete/:Id', getOrDelOneTaskVal, catchAsync(deleteTask));

module.exports = router;
