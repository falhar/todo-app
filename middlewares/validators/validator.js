const ApiError = require('../../errors/api-error');
const myValidationResult = require('../../services/myValidationResult');

function handleValidationError(req, res, next) {
  const errors = myValidationResult(req);

  if (!errors.isEmpty()) {
    next(new ApiError(200, 'Validation Error', 'error_param', errors.mapped({})));
  } else {
    next();
  }
}

module.exports = handleValidationError;
