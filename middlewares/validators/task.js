const { query, param, body } = require('express-validator');
const ApiError = require('../../errors/api-error');
const { task } = require('../../models');
const handleValidationError = require('./validator');

module.exports = {
  searchTaskVal: [
    query('Page').notEmpty().withMessage('page required').isInt({ gt: 0 }),
    query('Limit').notEmpty().withMessage('limit required').isInt({ gt: 0 }),
    query('Title').isString().optional(),
    query('Action_Time_Start')
      .trim()
      .customSanitizer((value) => new Date(parseInt(value, 10) * 1000))
      .isISO8601()
      .optional(),
    query('Action_Time_End')
      .trim()
      .customSanitizer((value) => new Date(parseInt(value, 10) * 1000))
      .isISO8601()
      .optional(),
    query('Is_Finished')
      .custom((value) => Boolean(value))
      .isBoolean()
      .optional(),
    handleValidationError,
  ],
  getOrDelOneTaskVal: [
    param('Id')
      .notEmpty()
      .withMessage('Id Task required')
      .custom(async (Task_ID) => {
        const countTask = await task.count({ where: { Task_ID } });
        if (!countTask) {
          throw new ApiError(200, 'task not found', 'error_id_not_found');
        } else {
          return true;
        }
      }),
    handleValidationError,
  ],
  addTaskVal: [
    body('Title').notEmpty().withMessage('Title required').isString(),
    body('Action_Time')
      .trim()
      .customSanitizer((value) => new Date(parseInt(value, 10) * 1000))
      .isISO8601()
      .optional(),
    body('Objective_List').notEmpty().withMessage('Objective_List required').isArray(),
    body('Objective_List.*').isLength({ min: 1 }).isString(),
    handleValidationError,
  ],
  updateTaskVal: [
    param('Id')
      .notEmpty()
      .withMessage('Id Task required')
      .custom(async (Task_ID) => {
        const countTask = await task.count({ where: { Task_ID } });
        if (!countTask) {
          throw new ApiError(200, 'task not found', 'error_id_not_found');
        } else {
          return true;
        }
      }),
    body('Title').notEmpty().withMessage('Title required').isString(),
    body('Objective_List').notEmpty().withMessage('Objective_List required').isArray(),
    body('Objective_List.*.Objective_Name').notEmpty().withMessage('Objective_Name required').isString(),
    body('Objective_List.*.Is_Finished').notEmpty().withMessage('Is_Finished required').isBoolean(),
    handleValidationError,
  ],
};
