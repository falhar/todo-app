const config = require('../config');
const ApiError = require('../errors/api-error');
const logger = require('../tools/logger');

/**
 * @param {Error} err
 * @returns {ApiError}
 * */
const convertError = (err) => {
  if (err instanceof ApiError) {
    return err;
  }

  return new ApiError(500, err.message, err.error_key, {}, false);
};

// eslint-disable-next-line no-unused-vars
const errorMiddleware = (err, req, res, next) => {
  const error = convertError(err);

  let { message, error_key } = error;
  if (config.app.env === 'production' && !error.isOperational) {
    message = 'Internal Server Error';
    error_key = 'error_internal_server';
  }

  res.locals.errorMessage = message;

  if (!error.isOperational) {
    logger.error(error);
  }

  res.status(error.statusCode).json({
    message: 'Failed',
    error_key,
    error_message: message,
    errors_data: error.errorPayloads,
    ...(config.app.env !== 'production' && { stack: err.stack }),
  });
};

module.exports = errorMiddleware;
